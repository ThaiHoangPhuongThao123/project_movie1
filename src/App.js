
import { createBrowserHistory } from 'history';
import './App.css';

import HomePage from './Pages/Home/Home';
import HomeTemplate from './templates/HomeTemplate/HomeTemplate';
import { Route,  Routes } from 'react-router-dom';
import Contact from './Pages/Contact/Contact';
import New from './Pages/New/New';
import About from './Pages/About/About';
import Login from './Pages/Login/Login';
import Register from './Pages/Register/Register';
export const history = createBrowserHistory();
function App() {
  return (
   <Routes history={history}>
<Route>
   <Route path='/' element={<HomeTemplate contentPage={<HomePage/>}/>}/>
   <Route path='/contact' element={<HomeTemplate contentPage={<Contact/>}/>}/>
   <Route path='/new' element={<HomeTemplate contentPage={<New/>}/>}/>
   <Route path='/about' element={<HomeTemplate contentPage={<About/>}/>}/>
</Route>
<Route>
   <Route path='/login' element={<Login/>}/>
   <Route path='/register' element={<Register/>}/>
</Route>
   </Routes>
  );
}

export default App;
