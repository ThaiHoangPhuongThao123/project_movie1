import React, { useEffect } from "react";
import { Carousel } from "antd";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { getCarouselAction } from "../../redux/actions/CarouselActions";
const contentStyle = {
  height: "600px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  // backgroundRepeat: "no-repeat ",
  // backgroundPosition:"center",
  // backgroundSize:"100%"
};

export default function CarouselComponet() {
  const bannerArr = useSelector((state) => {
    return state.CarouselReducer.bannerArr;
  });
  const dispatch = useDispatch()
  // sẽ tự kích hoạt khi component load lên
  useEffect(
    () => { 
      const action = getCarouselAction();
      //1 action ={type, payload}
      // 2(phai cài middleware callbackFuntion(action))
      dispatch(action);
     }
    , []);

  const renderCarousel = () => {
    return bannerArr.map((item, index) => {
      return (
        <div key={index}>
          {/* <div style={{...contentStyle, backgroundImage: `url(${item.hinhAnh})`}} ></div> */}
          <div style={contentStyle}>
            <img
              src={item.hinhAnh}
              alt="carousel-img"
              className="w-full h-full object-cover "
            />
          </div>
        </div>
      );
    });
  };
  return (
    <div>
      <Carousel effect="fade">{renderCarousel()}</Carousel>
    </div>
  );
}

// axios({
//   baseURL:"https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachBanner",
//   method: "get",
//   headers:{
//     TokenCybersoft: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0NCIsIkhldEhhblN0cmluZyI6IjA5LzEyLzIwMjMiLCJIZXRIYW5UaW1lIjoiMTcwMjA4MDAwMDAwMCIsIm5iZiI6MTY3MjQxOTYwMCwiZXhwIjoxNzAyMjI3NjAwfQ.P5fJSMdFWDXkAXi_Hm7kZhuXoxo6xtTzIno_q6kp38I"
//   }
// })
// .then((res ) => {
//   console.log('res: ', res);

//  })
//  .catch((err ) => {
//   console.log('err: ', err);

//   })
