import React from "react";
import { NavLink } from "react-router-dom";
import style  from "./Header.css";
export default function Header() {
  return (
    <div className="fixed  text-white bg-black bg-opacity-40 w-screen z-10 py-5">
      <div className="container flex justify-between h-16 mx-auto ">
        <NavLink to={"/"} className="flex space-x-2 items-center">
          <img
            src="https://img.freepik.com/free-vector/movie-time-neon-sign-sign_24908-55555.jpg?size=626&ext=jpg&ga=GA1.1.528047770.1689599706&semt=ais"
            alt="Logo"
            className="w-16 h-16 object-cover"
          />
          <h2 className="text-pink-900 text-2xl font-bold">Movie</h2>
        </NavLink>

        <ul className="items-stretch hidden space-x-6 lg:flex">
          <li className="flex">
            <NavLink
              to={"/"}
              rel="noopener noreferrer"
              href="#"
              className={({isActive}) => { 
               return isActive? "border-b-2 border-white" : "style.headerNavLink"
               }}
            
            >
              Home
            </NavLink>
          </li>
          <li className="flex">
            <NavLink
              to={"/contact"}
              rel="noopener noreferrer"
              href="#"
              className={({isActive}) => { 
               return isActive? "border-b-2 border-white" : "style.headerNavLink"
               }}
            >
              Contact
            </NavLink>
          </li>
          <li className="flex">
            <NavLink
              to={"/about"}
              rel="noopener noreferrer"
              href="#"
              className={({isActive}) => { 
               return isActive? "border-b-2 border-white" : "style.headerNavLink"
               }}
            >
              About
            </NavLink>
          </li>
          <li className="flex">
            <NavLink
              to={"/new"}
              rel="noopener noreferrer"
              href="#"
               className={({isActive}) => { 
               return isActive? "border-b-2 border-white" : "style.headerNavLink"
               }}
            >
              New
            </NavLink>
          </li>
        </ul>
        <div className="items-center flex-shrink-0 hidden lg:flex">
          <button className="self-center px-8 py-3 rounded">Sign in</button>
          <button className="self-center px-8 py-3 font-semibold rounded dark:bg-violet-400 dark:text-gray-900">
            Sign up
          </button>
        </div>
        <button className="p-4 lg:hidden">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            className="w-6 h-6 dark:text-gray-100"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M4 6h16M4 12h16M4 18h16"
            />
          </svg>
        </button>
      </div>
    </div>
  );
}
