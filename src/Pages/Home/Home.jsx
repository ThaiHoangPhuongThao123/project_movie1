import React from 'react'
import MenuHome from './MenuHome/MenuHome'
import ListMovie from './ListMovie/ListMovie'

export default function HomePage() {
  return (
    <div className='space-y-5 container'>HomePage
    <ListMovie/>
      <MenuHome/>
    </div>
  )
}
