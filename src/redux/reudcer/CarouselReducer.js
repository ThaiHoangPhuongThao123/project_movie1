import { SET_CAROUSEL } from "../actions/Types/CarouselType";

const initialState = {
  bannerArr: [
    {
      maBanner: 1,
      maPhim: 1282,
      hinhAnh: "https://movienew.cybersoft.edu.vn/hinhanh/ban-tay-diet-quy.png",
    },
  ],
};

export const CarouselReducer = (state = initialState, { type, payload }) => {
  switch (type) {

    case SET_CAROUSEL: {
      state.bannerArr = payload;
      return{...state}
    }
    default:
      return state;
  }
};
