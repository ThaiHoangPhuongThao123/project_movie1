import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import { CarouselReducer } from "../reudcer/CarouselReducer";

 const rootReducer = combineReducers({
    // state ứng dụng 
    CarouselReducer : CarouselReducer,
})

export const store = createStore(rootReducer, applyMiddleware(thunk))