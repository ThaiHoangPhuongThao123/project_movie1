


// callback function sử dụng để truyền tham sôs

import axios from "axios";
import { SET_CAROUSEL } from "./Types/CarouselType";
import {  qlPhimServices } from "../../Services/QuanLyPhimServices";

export const getCarouselAction = () => { 
    return async (dispatch) => {
    try {
      const resuft = await qlPhimServices.layDanhSachBanner();
      console.log('resuft: ', resuft);
      // đưa date lên redux
      dispatch({
        type:SET_CAROUSEL,
        payload: resuft.data.content,
      })

    } catch (err) {
      console.log("err", err);
    }
  }
 }