// import React from 'react'
// import { Route } from 'react-router-dom';

// const HomeTemplate = (props) => {// path exact component
//     const {Componet, ...restProps} = props;
//   return (
//         <Route {...restProps} render ={(propsRoute) => { 
//             // props.location, props.history, props.match => để get, set, ... param trên URL

//             return <>
//             <h2 className='bg-pink-500'></h2>
//             <Componet {...propsRoute}/>
//             </>
//          }}
//         />

//   )
// }

// export default HomeTemplate


import React from 'react'
import Header from '../../components/Header/Header'
import CarouselComponet from '../../components/CarouselComponet/CarouselComponet'
import Footer from '../../components/Footer/Footer'

export default function HomeTemplate({contentPage}) {
  return (
    <div className='space-x-3'>
      <Header/>
      <CarouselComponet/>
      {contentPage}
      <Footer/>
    </div>
  )
}
