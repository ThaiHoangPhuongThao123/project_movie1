import axios from "axios";
import { DOMAIN, TOKEN_MOVIE } from "../utils/settings/config";


export class baseService {
    // put json veef phias backend
     put = (url, model) => { 
        return axios({
            url: `${DOMAIN}+${url}`,
            method:"PUT",
            data: model,
            headers:{
                "TokenCybersoft": TOKEN_MOVIE,
            }
        })
     }
      get = (url) => { 
        return axios({
            url: `${DOMAIN}${url}`,
            method:"GET",
            headers:{
                "TokenCybersoft": TOKEN_MOVIE,
            }
        })
     }
      deleted = (url) => { 
        return axios({
            url: `${DOMAIN}+${url}`,
            method:"DELETE",
  
            headers:{
                "TokenCybersoft": TOKEN_MOVIE,
            }
        })
     }
      post = (url, model) => { 
        return axios({
            url: `${DOMAIN}+${url}`,
            method:"POST",
            data: model,
            headers:{
                "TokenCybersoft": TOKEN_MOVIE,
            }
        })
     }
 
}
