import { baseService } from "./baseService"

export class QuanLyPhimSevices extends baseService{
    constructor() {
        super();
    }
    layDanhSachBanner = () => { 
        return this.get("/api/QuanLyPhim/LayDanhSachBanner")
     }
}
export const  qlPhimServices = new QuanLyPhimSevices();
 